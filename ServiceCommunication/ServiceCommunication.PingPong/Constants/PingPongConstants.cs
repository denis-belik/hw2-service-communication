﻿
namespace ServiceCommunication.PingPong.Constants
{
	static public class PingPongConstants
	{
		public const string ExchangeName = "BetaExchange";

		public const string PingQueueName = "PingQueue";
		public const string PingRoutingKey = "PingKey";
		public const string FromPingerMessage = "Ping";

		public const string PongQueueName = "PongQueue";
		public const string PongRoutingKey = "PongKey";
		public const string FromPongerMessage = "Pong";
	}
}
