﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using ServiceCommunication.PingPong.Constants;
using RabbitMQ.Client.Events;
using ServiceCommunication.RabbitMQ.Wrapper.Services;
using ServiceCommunication.RabbitMQ.Wrapper.Factories;
using ServiceCommunication.RabbitMQ.Wrapper.Constants;
using ServiceCommunication.RabbitMQ.Wrapper.Models;
using RabbitMQ.Client;

namespace ServiceCommunication.PingPong.Services
{
	public class PingPongService : IDisposable
	{
		private readonly QueueService _queueService;

		public PingPongService(ConsumerSettings consumerSettings, IConnectionFactory connectionFactory)
		{
			_queueService = new QueueService(
				consumerSettings, 
				connectionFactory);

			_queueService.Received += OnReceived;
		}

		private void OnReceived(object sender, BasicDeliverEventArgs args)
		{
			var body = args.Body.ToArray();
			var message = Encoding.UTF8.GetString(body);
			PrintMessage(MessageConstants.QueueActions.Received, message, true);

			Thread.Sleep(2500);

			bool isMessageFromPinger = message.Contains(PingPongConstants.FromPingerMessage);

			string answerRoutingKey =
				isMessageFromPinger ?
				PingPongConstants.PingRoutingKey :
				PingPongConstants.PongRoutingKey;

			string answer =
				isMessageFromPinger ?
				PingPongConstants.FromPongerMessage :
				PingPongConstants.FromPingerMessage;

			_queueService.SendMessage(PingPongConstants.ExchangeName, answerRoutingKey, answer);
			PrintMessage(MessageConstants.QueueActions.Sent, answer, true);
		}

		public void StartPingPong(bool isPinger)
		{
			string routingKey =
				isPinger ?
				PingPongConstants.PongRoutingKey :
				PingPongConstants.PingRoutingKey;

			string message =
				isPinger ?
				PingPongConstants.FromPingerMessage :
				PingPongConstants.FromPongerMessage;

			_queueService.SendMessage(PingPongConstants.ExchangeName, routingKey, message);
			PrintMessage(MessageConstants.QueueActions.Sent, message, true);
		}

		public void PrintMessage(string queueAction, string message, bool loggingEnabled)
		{
			Console.WriteLine($"{queueAction} {message} {DateTime.Now}");
			if (loggingEnabled)
			{
				using (StreamWriter sw = new StreamWriter(@$"{queueAction}.txt", true))
				{
					sw.WriteLine($"{queueAction} {message} {DateTime.Now.TimeOfDay}");
				}
			}
		}

		public void Dispose()
		{
			_queueService.Dispose();
		}
	}
}
