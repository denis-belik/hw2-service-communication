﻿using ServiceCommunication.PingPong.Constants;
using ServiceCommunication.PingPong.Services;
using ServiceCommunication.RabbitMQ.Wrapper.Constants;
using ServiceCommunication.RabbitMQ.Wrapper.Factories;
using ServiceCommunication.RabbitMQ.Wrapper.Models;
using System;

namespace ServiceCommunication.Pinger
{
	class Pinger
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Pinger");

			PingPongService pingService = new PingPongService(
				new ConsumerSettings
				{
					ExchangeName = PingPongConstants.ExchangeName,
					ConsumerQueueName = PingPongConstants.PingQueueName,
					ConsumerRoutingKey = PingPongConstants.PingRoutingKey
				},
				ConnectionFactoryFactory.CreateConnectionFactory(
					MessageConstants.ConnectionFactory.DefaultHostname));

			pingService.StartPingPong(isPinger: true);

			Console.ReadKey();
			pingService.Dispose();
		}
	}
}
