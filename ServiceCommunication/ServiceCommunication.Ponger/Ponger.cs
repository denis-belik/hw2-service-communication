﻿using System;
using ServiceCommunication.RabbitMQ.Wrapper.Models;
using ServiceCommunication.PingPong.Constants;
using ServiceCommunication.PingPong.Services;
using ServiceCommunication.RabbitMQ.Wrapper.Factories;
using ServiceCommunication.RabbitMQ.Wrapper.Constants;

namespace ServiceCommunication.Ponger
{
	class Ponger
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Ponger");

			PingPongService pongService = new PingPongService(
				new ConsumerSettings
				{
					ExchangeName = PingPongConstants.ExchangeName,
					ConsumerQueueName = PingPongConstants.PongQueueName,
					ConsumerRoutingKey = PingPongConstants.PongRoutingKey
				},
				ConnectionFactoryFactory.CreateConnectionFactory(
					MessageConstants.ConnectionFactory.DefaultHostname));

			Console.ReadKey();
			pongService.Dispose();
		}
	}
}
