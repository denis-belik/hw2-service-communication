﻿
namespace ServiceCommunication.RabbitMQ.Wrapper.Constants
{
	public static class MessageConstants
	{
		public static class QueueActions
		{
			public const string Sent = "Sent";
			public const string Received = "Rsvd";
		}

		public static class ConnectionFactory
		{
			public const string DefaultHostname = "localhost";
			public const string DefaultUri = "amqp://guest:guest@localhost:5672";
		}
	}
}
