﻿using RabbitMQ.Client;
using System;

namespace ServiceCommunication.RabbitMQ.Wrapper.Factories
{
	public static class ConnectionFactoryFactory
	{
		public static IConnectionFactory CreateConnectionFactory(string hostname)
		{
			return new ConnectionFactory() { HostName = hostname };
		}

		public static IConnectionFactory CreateConnectionFactory(Uri uri)
		{
			return new ConnectionFactory() { Uri = uri };
		}
	}
}
