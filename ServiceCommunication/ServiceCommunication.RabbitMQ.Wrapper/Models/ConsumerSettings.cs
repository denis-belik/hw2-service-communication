﻿
namespace ServiceCommunication.RabbitMQ.Wrapper.Models
{
	public class ConsumerSettings
	{
		public string ExchangeName { get; set; }
		public string ConsumerQueueName { get; set; }
		public string ConsumerRoutingKey { get; set; }
	}
}
