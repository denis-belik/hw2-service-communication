﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceCommunication.RabbitMQ.Wrapper.Models;
using System;
using System.Text;

namespace ServiceCommunication.RabbitMQ.Wrapper.Services
{
	public class QueueService : IDisposable
	{
		private readonly ConsumerSettings _consumerSettings;

		private readonly IConnection _connection;
		private readonly IModel _channel;
		private EventingBasicConsumer _consumer;
		
		public event EventHandler<BasicDeliverEventArgs> Received
		{
			add => _consumer.Received += value;
			remove => _consumer.Received -= value;
		}

		public QueueService(ConsumerSettings settings, IConnectionFactory factory)
		{
			_consumerSettings = settings;

			_connection = factory.CreateConnection();
			_channel = _connection.CreateModel();

			BindConsumerQueue(settings);
			InitializeConsumer();
		}

		private void BindConsumerQueue(ConsumerSettings settings)
		{
			_channel.ExchangeDeclare(
				settings.ExchangeName,
				ExchangeType.Direct);

			_channel.QueueDeclare(
				queue: settings.ConsumerQueueName,
				durable: true,
				exclusive: false,
				autoDelete: false);

			_channel.QueueBind(
				queue: settings.ConsumerQueueName,
				exchange: settings.ExchangeName,
				routingKey: settings.ConsumerRoutingKey);
		}

		private void InitializeConsumer()
		{
			_consumer = new EventingBasicConsumer(_channel);
			_consumer.Received += OnReceivedBaseLogic;
			_channel.BasicConsume(
				queue: _consumerSettings.ConsumerQueueName, 
				autoAck: false, 
				consumer: _consumer);
		}

		private void OnReceivedBaseLogic(object sender, BasicDeliverEventArgs args)
		{
			_channel.BasicAck(args.DeliveryTag, false);
		}

		public void SendMessage(string exchangeName, string routingKey, string message)
		{
			var body = Encoding.UTF8.GetBytes(message);

			_channel.BasicPublish(
				exchange: _consumerSettings.ExchangeName,
				routingKey: routingKey,
				body: body);
		}

		public void Dispose()
		{
			_channel?.Dispose();
			_connection?.Dispose();
		}
	}
}
